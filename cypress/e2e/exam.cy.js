
/// <reference types="cypress" />

const url = 'https://65d23f04987977636bfc2429.mockapi.io'

var result = ''

describe('clear', () => {

  beforeEach(() => {
        cy.clearAllLocalStorage()
        cy.visit(url)
        cy.wait(1000)     
        
  })
    
});

it('update',() => {
      cy.request('PUT', url+'/ep1/5', { 
        
              ProductName: "Battery 1",
              Fullname: "Sakarin Juajan",
              Total: 25470,
              freedelivery: true, //true is free delivery voucher ,false is not
              promptpay: true, //true is promptpay ,false is not

      })

      it('successfully', () => {
        cy.log('update success')
      })
}) 

it('Get data check',() => {

  cy.request({
    method: 'GET', 
    url: url+'/ep1/5', 
  }).then( ({ body,status }) => {

      result = body.freedelivery && body.promptpay

          //assert status 200
            expect(status).to.eq(200)

            if (body.freedelivery == true && body.promptpay == true) {
              
            // assert, Pass here user use free deliver voucher and payment promptpay.
            expect(result).to.eq(true)
            
          }else {          
            // Fail here user not use free deliver voucher and payment promptpay.
            expect(result).to.eq(true)
          }

  })
  // cy.visit(url)
  cy.wait(2000) 
}) 